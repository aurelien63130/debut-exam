package com.hb.spring.coursenplus.service;

import com.hb.spring.coursenplus.DataLoaderApplication;
import com.hb.spring.coursenplus.model.User;
import com.hb.spring.coursenplus.repository.UserRepository;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService implements UserServiceInterface {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(UserService.class);

    public void create(String username, String password){
        User user = new User();
        user.setUsername(username);
        user.setPassword(this.passwordEncoder.encode(password));

        this.userRepository.save(user);
        log.info(passwordEncoder.encode(password));
    }

    public User findByUsername(String username){
        return this.userRepository.findByUsername(username);
    }

    public User save(User registration) {
        User user = new User();

        user.setUsername(registration.getUsername());
        user.setPassword(passwordEncoder.encode(registration.getPassword()));


        System.out.println();
        System.out.println("Password is         : " + registration.getPassword());
        System.out.println("Encoded Password is : " + passwordEncoder.encode(registration.getPassword()));
        System.out.println();

        return userRepository.save(user);
    }


    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(email);
        if (user == null) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }

        ArrayList<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

        SimpleGrantedAuthority sa = new SimpleGrantedAuthority("USER");

        authorities.add(sa);


        return new org.springframework.security.core.userdetails.User(user.getUsername(),
                user.getPassword(),
                authorities);
    }

    public List<User> findAll(){
        return userRepository.findAll();
    }

}
