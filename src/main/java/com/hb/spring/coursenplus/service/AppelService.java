package com.hb.spring.coursenplus.service;

import com.hb.spring.coursenplus.DataLoaderApplication;
import com.hb.spring.coursenplus.model.Appel;
import com.hb.spring.coursenplus.model.TypeAppelant;
import com.hb.spring.coursenplus.model.User;
import com.hb.spring.coursenplus.repository.AppelRepository;
import com.hb.spring.coursenplus.repository.TypeAppelantRepository;
import com.hb.spring.coursenplus.repository.UserRepository;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class AppelService {

    @Autowired
    private AppelRepository appelRepository;

    @Autowired UserService userService;

    public void save(Appel appel){
        LocalDateTime today = LocalDateTime.now();
        appel.setDateHeureDebut(today);

        Object authentication = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        String username = ((UserDetails) authentication).getUsername();

        User user = userService.findByUsername(username);
        appel.setUser(user);

        this.appelRepository.save(appel);
    }

    public List<Appel> getAll(){
        return (List<Appel>) this.appelRepository.findAll();
    }
}
