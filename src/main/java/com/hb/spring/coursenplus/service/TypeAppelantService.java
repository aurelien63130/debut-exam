package com.hb.spring.coursenplus.service;

import com.hb.spring.coursenplus.DataLoaderApplication;
import com.hb.spring.coursenplus.model.TypeAppelant;
import com.hb.spring.coursenplus.model.User;
import com.hb.spring.coursenplus.repository.TypeAppelantRepository;
import com.hb.spring.coursenplus.repository.UserRepository;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TypeAppelantService {

    @Autowired
    private TypeAppelantRepository typeAppelantRepository;

    public void create(String libelle){
        TypeAppelant ta = new TypeAppelant();
        ta.setLibelle(libelle);

        typeAppelantRepository.save(ta);
    }

    public List<TypeAppelant> getAll(){
        return (List<TypeAppelant>) typeAppelantRepository.findAll();
    }
}
