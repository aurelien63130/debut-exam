package com.hb.spring.coursenplus.repository;

import com.hb.spring.coursenplus.model.Appel;
import com.hb.spring.coursenplus.model.TypeAppelant;
import com.hb.spring.coursenplus.model.User;
import org.springframework.data.repository.CrudRepository;

public interface AppelRepository extends CrudRepository<Appel, Long> {

}
