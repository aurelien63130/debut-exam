package com.hb.spring.coursenplus.controller;

import com.hb.spring.coursenplus.model.Appel;
import com.hb.spring.coursenplus.service.AppelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;


@Controller
public class HomeController {

    @Autowired
    AppelService appelService;

    @GetMapping("/")
    public ModelAndView index() {
        List<Appel> appels = this.appelService.getAll();

        ModelAndView mv = new ModelAndView("home");
        mv.addObject("appels", appels);

        return mv;
    }




}
