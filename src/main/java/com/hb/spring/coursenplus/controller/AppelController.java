package com.hb.spring.coursenplus.controller;

import com.hb.spring.coursenplus.model.Appel;
import com.hb.spring.coursenplus.model.TypeAppelant;
import com.hb.spring.coursenplus.model.User;
import com.hb.spring.coursenplus.service.AppelService;
import com.hb.spring.coursenplus.service.TypeAppelantService;
import com.hb.spring.coursenplus.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;


@Controller
public class AppelController {

    @Autowired
    private UserService userService;

    @Autowired
    private TypeAppelantService typeAppelantService;

    @Autowired
    private AppelService appelService;

    @GetMapping("/add")
    public ModelAndView add() {
        List<User> users = userService.findAll();
        List<TypeAppelant> typeAppelants = this.typeAppelantService.getAll();
        Appel appel = new Appel();
        ModelAndView mv = new ModelAndView("add");
        mv.addObject("users", users);
        mv.addObject("appel", appel);
        mv.addObject("typeAppelants", typeAppelants);

        return mv;
    }

    @PostMapping("/add")
    public ModelAndView processForm(@Valid Appel appel, BindingResult br) {
        if(br.hasErrors())
        {
            List<User> users = userService.findAll();
            List<TypeAppelant> typeAppelants = this.typeAppelantService.getAll();

            ModelAndView mv = new ModelAndView("add");
            mv.addObject("users", users);
            mv.addObject("appel", appel);
            mv.addObject("typeAppelants", typeAppelants);

            return mv;
        }
        else
        {
            this.appelService.save(appel);
            return new ModelAndView("redirect:/");
        }
    }

}
