package com.hb.spring.coursenplus;

import com.hb.spring.coursenplus.model.TypeAppelant;
import com.hb.spring.coursenplus.model.User;
import com.hb.spring.coursenplus.repository.TypeAppelantRepository;
import com.hb.spring.coursenplus.repository.UserRepository;
import com.hb.spring.coursenplus.service.TypeAppelantService;
import com.hb.spring.coursenplus.service.UserService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.List;

@SpringBootApplication
public class DataLoaderApplication {

    @Autowired
    private UserService userService;

    @Autowired
    private TypeAppelantService typeAppelantService;

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(DataLoaderApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(DataLoaderApplication.class);
    }

    @Bean
    public CommandLineRunner PreLoadDataApplication(UserRepository UserRepository,
                                                    TypeAppelantRepository typeAppelantRepository) {
        return (args) -> {
            List<User> users = (List<User>) UserRepository.findAll();
            if(users.size() == 0){
                this.userService.create("toto", "toto");
                this.userService.create("tata", "tata");
            }

            List<TypeAppelant> typeAppelants = (List<TypeAppelant>) typeAppelantRepository.findAll();

            if(typeAppelants.size() == 0){
                this.typeAppelantService.create("vendeur");
                this.typeAppelantService.create("acquereur");
            }

        };
    };



}
