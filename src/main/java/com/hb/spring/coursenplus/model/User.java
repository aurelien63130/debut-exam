package com.hb.spring.coursenplus.model;

import com.sun.istack.NotNull;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Basic
    private String username;

    @Basic
    private String password;

    @OneToMany(mappedBy = "user")
    private List<Appel> appels;

    public User() {
        this.appels = new ArrayList<>();
    }

    public void addAppel(Appel appel){
        this.appels.add(appel);
    }

    public void removeAppel(Appel appel){
        this.appels.remove(appel);
    }

    public List<Appel> getAppels(){
        return this.appels;
    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

}
