package com.hb.spring.coursenplus.model;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Appel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private LocalDateTime dateHeureDebut;

    @Column(nullable = false)
    @NotNull()
    @NotBlank(message = "Veuillez saisir un nom")
    private String nomAppelant;

    @Column(nullable = false)
    @NotNull()
    @NotBlank(message = "Veuillez saisir un prénom")
    private String prenomAppelant;

    @Column()
    @NotBlank(message = "Veuillez saisir un numéro de tel")
    private String telAppelant;

    @ManyToOne()
    @JoinColumn( name="id_user" )
    private User user;

    @ManyToMany()
    private List<User> employeesConcernes;


    @ManyToOne()
    @JoinColumn( name="id_support" )
    private SupportPublicitaire supportPublicitaire;


    @ManyToOne()
    @NotNull()
    @Valid
    @JoinColumn( name="type_appelant_id" )
    private TypeAppelant typeAppelant;

    public Appel(){
        this.employeesConcernes = new ArrayList<>();
    }

    public List<User> getEmployeesConcernes(){

        return this.employeesConcernes;
    }

    public void addEmployeesConcerne(User employeeConcerne){
        this.employeesConcernes.add(employeeConcerne);
    }

    public void removeEmployeeConcerne(User user){
        this.employeesConcernes.remove(user);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypeAppelant getTypeAppelant() {
        return typeAppelant;
    }

    public void setTypeAppelant(TypeAppelant typeAppelantId) {
        this.typeAppelant = typeAppelantId;
    }

    public LocalDateTime getDateHeureDebut() {
        return dateHeureDebut;
    }

    public void setDateHeureDebut(LocalDateTime dateHeureDebut) {
        this.dateHeureDebut = dateHeureDebut;
    }

    public String getNomAppelant() {
        return nomAppelant;
    }

    public void setNomAppelant(String nomAppelant) {
        this.nomAppelant = nomAppelant;
    }

    public String getPrenomAppelant() {
        return prenomAppelant;
    }

    public void setPrenomAppelant(String prenomAppelant) {
        this.prenomAppelant = prenomAppelant;
    }

    public String getTelAppelant() {
        return telAppelant;
    }

    public void setTelAppelant(String telApprenant) {
        this.telAppelant = telApprenant;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }



    public SupportPublicitaire getSupportPublicitaire() {
        return supportPublicitaire;
    }

    public void setSupportPublicitaire(SupportPublicitaire supportPublicitaire) {
        this.supportPublicitaire = supportPublicitaire;
    }

}
